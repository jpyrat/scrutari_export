<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Boite de configuration permettant d'exclure un article d'un export
 *
 * @param array $flux
 * @return array
 */
function scrutari_export_afficher_config_objet($flux){
	if ((($type = $flux['args']['type'])=='article')
	AND ($id = $flux['args']['id'])){
		if (autoriser('affecterstatutscrutari', $type, $id)){
			$table = table_objet($type);
			$id_table_objet = id_table_objet($type);
			$flux['data'] .= recuperer_fond("prive/objets/editer/affecter_statut_scrutari",array($id_table_objet=>$id));
		}
	}
	return $flux;
}

?>
