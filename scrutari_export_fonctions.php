<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * liste des objets contenus dans un export.
 *
 * @param int|string $id_exportscrutari
 * @return array
 */
function scrutariexport_liste_contenu_export($id_exportscrutari, $objet){
	$liste = array();

	$where = array();
	if (is_numeric($id_exportscrutari))
		$where[] = "e.id_exportscrutari=" . intval($id_exportscrutari);
	elseif ($id_exportscrutari) {
		$where = $id_exportscrutari;
	}

	if (is_array($where))
		$where[] = "el.objet=" . sql_quote($objet);
	else
		$where = "($where) AND el.objet=" . sql_quote($objet);

	include_spip('base/abstract_sql');
	$liste = sql_allfetsel('id_objet', 'spip_exportscrutari_liens AS el INNER JOIN spip_exportscrutari AS e ON el.id_exportscrutari=e.id_exportscrutari', $where);
	$liste = array_map('reset', $liste);
	$liste = array_unique($liste);
	return $liste;
}

/**
 * liste des rubriques contenues dans un corpus, directement.
 * pour savoir quelles rubriques on peut decocher
 * si id_zone = '' : toutes les rub en acces restreint
 *
 * @param int /string $id_zone
 * @return array
 */
function scrutariexport_liste_contenu_corpus_rub($id_corpus){
	$liste_rubriques = array();

	// liste des rubriques directement liees au corpus
	$where = array();
	if (is_numeric($id_corpus))
		$where[] = "c.id_corpus=" . intval($id_corpus);
	elseif ($id_corpus)
		$where = $id_corpus;

	if (is_array($where))
		$where[] = "cl.objet='rubrique'";
	else
		$where = "($where) AND cl.objet='rubrique'";

	include_spip('base/abstract_sql');
	$liste_rubriques = sql_allfetsel('id_objet', 'spip_corpus_liens AS cl INNER JOIN spip_corpus AS c ON cl.id_corpus=c.id_corpus', $where);
	$liste_rubriques = array_map(fn($item) => $item['id_objet'], $liste_rubriques);
	$liste_rubriques = array_unique($liste_rubriques);
	return $liste_rubriques;
}

/**
 * liste des groupes de mots contenues dans un thesaurus, directement.
 * pour savoir quelles groupes on peut decocher
 *
 * @param int /string $id_thesaurus
 * @return array
 */
function scrutariexport_liste_contenu_thesaurus_groupes($id_thesaurus){
	$liste_groupes = array();

	$where = array();
	if (is_numeric($id_thesaurus))
		$where[] = "t.id_thesaurus=" . intval($id_thesaurus);
	elseif ($id_thesaurus) {
		$where = $id_thesaurus;
	}

	if (is_array($where))
		$where[] = "tl.objet='groupe'";
	else
		$where = "($where) AND tl.objet='groupe'";

	include_spip('base/abstract_sql');
	$liste_groupes = sql_allfetsel('id_objet', 'spip_thesaurus_liens AS tl INNER JOIN spip_thesaurus AS t ON tl.id_thesaurus=t.id_thesaurus', $where);
	$liste_groupes = array_map('reset', $liste_groupes);
	$liste_groupes = array_unique($liste_groupes);
	return $liste_groupes;
}

?>
