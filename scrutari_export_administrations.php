<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/meta');
/**
 * Fonction d'installation, mise a jour de la base
 *
 * @param unknown_type $nom_meta_base_version
 * @param unknown_type $version_cible
 */
function scrutari_export_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array(
			'spip_exportscrutari',
			'spip_exportscrutari_articles_exclus',
			'spip_exportscrutari_liens',
			'spip_corpus',
			'spip_corpus_liens',
			'spip_thesaurus',
			'spip_thesaurus_liens'
		)),
	);
	$maj['0.2.0'] = array(
		array('sql_alter', 'TABLE spip_articles DROP statut_scrutari'),
		array('sql_drop_table', 'spip_statutscrutari'),
		array('sql_drop_table', 'spip_exportscrutari_statutscrutari')
	);
	$maj['0.3.0'] = array(
		array('maj_liens_scrutari_export', 'exportscrutari'),
		array('maj_liens_scrutari_export', 'exportscrutari', 'corpus'),
		array('sql_drop_table', "spip_exportscrutari_corpus"),
		array('maj_liens_scrutari_export', 'exportscrutari', 'thesaurus'),
		array('sql_drop_table', "spip_exportscrutari_thesaurus"),

		array('maj_liens_scrutari_export', 'corpus'),
		array('maj_liens_scrutari_export', 'corpus', 'rubrique'),
		array('sql_drop_table', "spip_corpus_rubriques"),

		array('maj_liens_scrutari_export', 'thesaurus'),
		array('maj_liens_scrutari_export', 'thesaurus', 'groupe'),
		array('sql_drop_table', "spip_thesaurus_groupes"),
	);
	$maj['0.3.1'] = array(
		array('maj_tables', array(
			'spip_exportscrutari_articles_exclus',
		)),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

}

/**
 * Fonction de desinstallation
 *
 * @param unknown_type $nom_meta_base_version
 */
function scrutari_export_vider_tables($nom_meta_base_version){
	sql_drop_table("spip_corpus");
	sql_drop_table("spip_corpus_liens");
	sql_drop_table("spip_thesaurus");
	sql_drop_table("spip_thesaurus_liens");
	sql_drop_table("spip_exportscrutari");
	sql_drop_table("spip_exportscrutari_liens");
	sql_drop_table("spip_exportscrutari_articles_exclus");

	effacer_meta($nom_meta_base_version);
}

function maj_liens_scrutari_export($pivot, $l = ''){

	$exceptions_pluriel = array('exportscrutari' => 'exportscrutari', 'corpus' => 'corpus', 'thesaurus' => 'thesaurus');

	$pivot = preg_replace(',[^\w],', '', $pivot); // securite
	$pivots = (isset($exceptions_pluriel[$pivot]) ? $exceptions_pluriel[$pivot] : $pivot . "s");
	$liens = "spip_" . $pivots . "_liens";
	$id_pivot = "id_" . $pivot;
	// Creer spip_auteurs_liens
	global $tables_auxiliaires;
	if (!$l){
		include_spip('base/auxiliaires');
		include_spip('base/create');
		creer_ou_upgrader_table($liens, $tables_auxiliaires[$liens], false);
	} else {
		// Preparer
		$l = preg_replace(',[^\w],', '', $l); // securite
		$primary = "id_$l";
		$objet = $l;
		$ls = (isset($exceptions_pluriel[$l]) ? $exceptions_pluriel[$l] : $l . "s");
		$ancienne_table = 'spip_' . $pivots . '_' . $ls;
		$pool = 400;

		$trouver_table = charger_fonction('trouver_table', 'base');
		if (!$desc = $trouver_table($ancienne_table))
			return;

		$champs = $desc['field'];
		if (isset($champs['maj'])) unset($champs['maj']);
		if (isset($champs[$primary])) unset($champs[$primary]);

		$champs = array_keys($champs);
		// ne garder que les champs qui existent sur la table destination
		if ($desc_cible = $trouver_table($liens)){
			$champs = array_intersect($champs, array_keys($desc_cible['field']));
		}

		$champs[] = "$primary as id_objet";
		$champs[] = "'$objet' as objet";
		$champs = implode(', ', $champs);

		// Recopier les donnees
		$sub_pool = 100;
		while ($ids = array_map('reset', sql_allfetsel("$primary", $ancienne_table, '', '', '', "0,$sub_pool"))){
			$insert = array();
			foreach ($ids as $id){
				$n = sql_countsel($liens, "objet='$objet' AND id_objet=" . intval($id));
				while ($t = sql_allfetsel($champs, $ancienne_table, "$primary=" . intval($id), '', $id_pivot, "$n,$pool")){
					$n += count($t);
					// empiler en s'assurant a minima de l'unicite
					while ($r = array_shift($t))
						$insert[$r[$id_pivot] . ':' . $r['id_objet']] = $r;
					if (count($insert)>=$sub_pool){
						maj_liens_scrutari_export_insertq_multi_check($liens, $insert, $tables_auxiliaires[$liens]);
						$insert = array();
					}
					// si timeout, sortir, la relance nous ramenera dans cette fonction
					// et on verifiera/repartira de la
					if (time()>=_TIME_OUT) return;
				}
				if (time()>=_TIME_OUT) return;
			}
			if (count($insert))
				maj_liens_scrutari_export_insertq_multi_check($liens, $insert, $tables_auxiliaires[$liens]);
			sql_delete($ancienne_table, sql_in($primary, $ids));
		}
	}
}

function maj_liens_scrutari_export_insertq_multi_check($table, $couples, $desc = array()){
	$n_before = sql_countsel($table);
	sql_insertq_multi($table, $couples, $desc);
	$n_after = sql_countsel($table);
	if (($n_after-$n_before)==count($couples))
		return;
	// si ecart, on recommence l'insertion ligne par ligne...
	// moins rapide mais secure : seul le couple en doublon echouera, et non toute la serie
	foreach ($couples as $c)
		sql_insertq($table, $c, $desc);
}

?>
