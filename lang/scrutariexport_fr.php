<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'affecter_corpus' => 'Affecter à un corpus',
	'auteur_spip' => 'Traiter les auteurs Spip comme un champ complémentaire',
	'authority_uuid' => 'Identifiant unique universel',

	'base_name' => 'Nom de la base',

	'confirmer_supprimer_corpus' => 'Êtes vous s&ucirc;r de vouloir supprimer ce corpus ?',
	'confirmer_supprimer_export' => 'Êtes vous s&ucirc;r de vouloir supprimer cet export ?',
	'confirmer_supprimer_thesaurus' => 'Êtes vous s&ucirc;r de vouloir supprimer ce thésaurus ?',
	'corpus_de_l_export' => 'Corpus de l\'export',
	'creer_corpus' => 'Créer un nouveau corpus',
	'creer_export' => 'Créer un nouvel export',
	'creer_thesaurus' => 'Créer un nouveau thésaurus',

	'definition_export_scrutari' => 'Définition de l\'export Scrutari',

	'exclure_de_l_export' => 'Exclure de l\'export&nbsp;: ',

	'format_non_conforme' => 'Format non conforme',

	'gerer_corpus' => 'Gérer les corpus',
	'gerer_exports' => 'Gérer les exports',
	'gerer_thesaurus' => 'Gérer les thésaurus',
	'groupes_thesaurus' => 'groupes de mots dans le thésaurus',
	'groupes_du_thesaurus' => 'Groupes de mots du thésaurus',

	'icone_menu_config' => 'Export Scrutari',
	'icone_supprimer_corpus' => 'Supprimer ce corpus',
	'icone_supprimer_export' => 'Supprimer cet export',
	'icone_supprimer_thesaurus' => 'Supprimer ce thésaurus',
	'info_aucun_corpus' => 'Rubrique affectée à aucun corpus',
	'info_page_corpus' => 'Cette page vous permet de gérer les corpus',
	'info_page_exports' => 'Cette page vous permet de gérer les exports pour Scrutari.',
	'info_page_thesaurus' => 'Cette page vous permet de gérer les thésaurus',
	'info_retirer_corpus' => 'Retirer du corpus',
	'intitule_champ_auteur' => 'Intitulé du champ auteur',
	'intitule_corpus' => 'Intitulé du corpus',
	'intitule_long' => 'Intitulé long',
	'intitule_thesaurus' => 'Intitulé du thésaurus',
	'intitule_fiche' => 'Intitulé d’un article',
	'intitule_short' => 'Intitulé court',

	'liste_corpus' => 'Liste des corpus',
	'liste_exports' => 'Liste des exports',
	'liste_thesaurus' => 'Liste des thésaurus',

	'modifier_corpus' => 'Modifier un corpus',
	'modifier_export' => 'Modifier un export',
	'modifier_thesaurus' => 'Modifier un thésaurus',


	'nom_champ_auteur' => 'auteur',
	'nom_deja_existant' => 'Nom déjà défini',
	'nom_technique' => 'Nom technique',

	'rubriques_corpus' => 'rubriques dans le corpus',
	'rubriques_du_corpus' => 'Rubriques du corpus',

	'statut_scrutari' => 'Statut Scrutari de l\'article',
	'selectionner_un_corpus' => 'Sélectionner un corpus',

	'thesaurus_de_l_export' => 'Thésaurus de l\'export',
	'titre_scrutariexport' => 'Export Scrutari',

	'' => ''
);

?>
