# Export Scrutari #

### Le moteur de recherche Scrutari ###

Scrutari est un moteur de recherche destiné à effectuer des recherches sur un nombre précis de sites (les sites « abonnés »). Sa particularité est de baser sa recherche sur les méta-données transmises par les sites abonnés et non sur les documents des site eux-mêmes.

[La documentation en ligne de Scrutari](http://www.scrutari.net).

### Le format ScrutariData ###

Le format ScrutariData est un format de description des méta-données d'un site. Ce format fait la part belle aux titres et sous-titres et à l'indexation par des mots-clés. C'est un format défini en XML ; il peut donc être implémenté pour n'importe quel site et quelque soit le langage de programmation (comme le sont les flux RSS).

[Description du format ScrutariData](http://www.scrutari.net/dokuwiki/scrutaridata:xml).

### Le plugin Export Scrutari pour SPIP ###

Le plugin permet de définir un ou plusieurs exports au format ScrutariData, ainsi que les corpus et les thésaurus qui les composent. Les corpus sont composés d'une ou plusieurs rubriques, les thésaurus sont composés d'un ou plusieurs groupes de mots clés.

Il s’installe comme tous les autres plugins SPIP.

La définition des exports, des corpus et des thésaurus est accesible pour les administrateurs dans Configuration / Export Scrutari.