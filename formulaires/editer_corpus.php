<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_editer_corpus_identifier_dist($id_corpus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_corpus)));
}

function formulaires_editer_corpus_charger_dist($id_corpus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){
	$valeurs = formulaires_editer_objet_charger('corpus', $id_corpus, 0, 0, $retour, $config_fonc, $row, $hidden);

	// charger les rubriques associees au corpus
	if ($id_corpus = intval($id_corpus)){
		$valeurs['rubriques'] = scrutariexport_liste_contenu_corpus_rub($id_corpus);
	} else {
		$valeurs['rubriques'] = '';
	}

	if (!$valeurs['intitule_champ_auteur']){
		$valeurs['intitule_champ_auteur'] = _T('scrutariexport:nom_champ_auteur');
	}

	return $valeurs;
}

function formulaires_editer_corpus_verifier_dist($id_corpus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){
	$erreurs = formulaires_editer_objet_verifier('corpus', $id_corpus, array('name', 'intitule_corpus', 'intitule_fiche'));

	if (!_request('intitule_champ_auteur')){
		set_request('intitule_champ_auteur', _T('scrutariexport:nom_champ_auteur'));
	}

	if (!$erreurs['name']){
		if (!preg_match('#^[a-z][a-z0-9_]*$#', _request('name'))){
			$erreurs['name'] = _T('scrutariexport:format_non_conforme');
		} else {
			if (is_numeric($id_corpus) && sql_getfetsel('name', 'spip_corpus', 'name="' . _request('name') . '" AND id_corpus!=' . $id_corpus)){
				$erreurs['name'] = _T('scrutariexport:nom_deja_existant');
			}
			if (!is_numeric($id_corpus) && sql_getfetsel('name', 'spip_corpus', 'name="' . _request('name') . '"')){
				$erreurs['name'] = _T('scrutariexport:nom_deja_existant');
			}
		}
	}

	return $erreurs;
}

function formulaires_editer_corpus_traiter_dist($id_corpus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){

	if (_request('auteur_spip')!='oui'){
		set_request('auteur_spip', 'non');
	}

	$res = formulaires_editer_objet_traiter('corpus', $id_corpus, 0, 0, $retour, $config_fonc, $row, $hidden);

	if ($retour AND $res['id_corpus']){
		$res['redirect'] = parametre_url($retour, 'id_corpus', $res['id_corpus']);
	}

	return $res;

}

?>
