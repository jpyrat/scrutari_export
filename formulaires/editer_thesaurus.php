<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_editer_thesaurus_identifier_dist($id_thesaurus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_thesaurus)));
}

function formulaires_editer_thesaurus_charger_dist($id_thesaurus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){
	$valeurs = formulaires_editer_objet_charger('thesaurus', $id_thesaurus, 0, 0, $retour, $config_fonc, $row, $hidden);

	// charger les rubriques associees au corpus
	if ($id_thesaurus = intval($id_thesaurus)){
		$valeurs['groupes'] = scrutariexport_liste_contenu_thesaurus_groupes($id_thesaurus);
	}

	return $valeurs;
}

function formulaires_editer_thesaurus_verifier_dist($id_thesaurus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){
	$erreurs = formulaires_editer_objet_verifier('thesaurus', $id_thesaurus, array('name', 'intitule_thesaurus'));


	if (!$erreurs['name']){
		if (!preg_match('#^[a-z][a-z0-9_]*$#', _request('name'))){
			$erreurs['name'] = _T('scrutariexport:format_non_conforme');
		} else {
			if (is_numeric($id_thesaurus) && sql_getfetsel('name', 'spip_thesaurus', 'name="' . _request('name') . '" AND id_thesaurus!=' . $id_thesaurus)){
				$erreurs['name'] = _T('scrutariexport:nom_deja_existant');
			}
			if (!is_numeric($id_thesaurus) && sql_getfetsel('name', 'spip_thesaurus', 'name="' . _request('name') . '"')){
				$erreurs['name'] = _T('scrutariexport:nom_deja_existant');
			}
		}
	}

	return $erreurs;
}

function formulaires_editer_thesaurus_traiter_dist($id_thesaurus = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){

	$res = formulaires_editer_objet_traiter('thesaurus', $id_thesaurus, 0, 0, $retour, $config_fonc, $row, $hidden);

	if ($retour AND $res['id_thesaurus']){
		$res['redirect'] = parametre_url($retour, 'id_thesaurus', $res['id_thesaurus']);
	}

	return $res;


}

?>
