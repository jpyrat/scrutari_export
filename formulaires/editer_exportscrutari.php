<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_editer_exportscrutari_identifier_dist($id_exportscrutari = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_exportscrutari)));
}

function formulaires_editer_exportscrutari_charger_dist($id_exportscrutari = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){

	$valeurs = formulaires_editer_objet_charger('exportscrutari', $id_exportscrutari, 0, 0, $retour, $config_fonc, $row, $hidden);

	if (!$valeurs['authority_uuid']){
		$valeurs['authority_uuid'] = '7121aba0-5232-11e1-b86c-0800200c9a66';
	}

	if ($id_exportscrutari = intval($id_exportscrutari)){
		// charger les corpus associees a l'export
		$valeurs['corpus'] = scrutariexport_liste_contenu_export($id_exportscrutari, 'corpus');
		// charger les thesaurus associees a l'export
		$valeurs['thesaurus'] = scrutariexport_liste_contenu_export($id_exportscrutari, 'thesaurus');
	} else {
		$valeurs['corpus'] = $valeurs['thesaurus'] = '';
	}

	return $valeurs;
}

function formulaires_editer_exportscrutari_verifier_dist($id_exportscrutari = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){

	$erreurs = formulaires_editer_objet_verifier('exportscrutari', $id_exportscrutari, array('authority_uuid', 'base_name', 'intitule_short', 'intitule_long'));

	if (!$erreurs['base_name']){
		if (!preg_match('#^[a-z][a-z0-9_]*$#', _request('base_name'))){
			$erreurs['base_name'] = _T('scrutariexport:format_non_conforme');
		} else {
			if (is_numeric($id_exportscrutari) && sql_getfetsel('base_name', 'spip_exportscrutari', 'base_name="' . _request('base_name') . '" AND id_exportscrutari!=' . $id_exportscrutari)){
				$erreurs['base_name'] = _T('scrutariexport:nom_deja_existant');
			}
			if (!is_numeric($id_exportscrutari) && sql_getfetsel('base_name', 'spip_exportscrutari', 'base_name="' . _request('base_name') . '"')){
				$erreurs['base_name'] = _T('scrutariexport:nom_deja_existant');
			}
		}
	}

	return $erreurs;
}

function formulaires_editer_exportscrutari_traiter_dist($id_exportscrutari = 'new', $retour = '', $config_fonc = '', $row = array(), $hidden = ''){

	$res = formulaires_editer_objet_traiter('exportscrutari', $id_exportscrutari, 0, 0, $retour, $config_fonc, $row, $hidden);

	if ($retour AND $res['id_exportscrutari']){
		$res['redirect'] = parametre_url($retour, 'id_exportscrutari', $res['id_exportscrutari']);
	}

	return $res;

}

?>
