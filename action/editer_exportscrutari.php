<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * editer un export (action apres creation/modif de export)
 *
 * @return array
 */
function action_editer_exportscrutari_dist($arg = null){

	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!$id_exportscrutari = intval($arg)){
		$id_exportscrutari = exportscrutari_inserer();
	}

	$err = exportscrutari_modifier($id_exportscrutari);

	return array($id_exportscrutari, $err);
}

/**
 * Creer un nouveau export
 *
 * @return int
 */
function exportscrutari_inserer(){

	include_spip('inc/autoriser');
	if (!autoriser('administrer', 'scrutariexport'))
		return false;

	$champs = array();
	// Envoyer aux plugins
	$champs = pipeline('pre_insertion',
		array(
			'args' => array(
				'table' => 'spip_exportscrutari',
			),
			'data' => $champs
		)
	);
	$id_exportscrutari = sql_insertq("spip_exportscrutari", $champs);
	pipeline('post_insertion',
		array(
			'args' => array(
				'table' => 'spip_exportscrutari',
				'id_objet' => $id_exportscrutari
			),
			'data' => $champs
		)
	);
	return $id_exportscrutari;
}

/**
 * Modifier un export
 *
 * @param int $id_export
 * @param array $set
 * @return string|bool
 */
function exportscrutari_modifier($id_exportscrutari, $set = null){

	include_spip('inc/modifier');
	$c = collecter_requests(
	// white list
		array('authority_uuid', 'base_name', 'intitule_short', 'intitule_long'),
		// black list
		array(),
		// donnees eventuellement fournies
		$set
	);

	if ($err = objet_modifier_champs('exportscrutari', $id_exportscrutari,
		array(),
		$c)
	)
		return $err;

	exportscrutari_lier($id_exportscrutari, 'corpus', _request('corpus'), 'set');
	exportscrutari_lier($id_exportscrutari, 'thesaurus', _request('thesaurus'), 'set');
	return $err;

}

/**
 * Mettre a jour les liens objets/exportscrutari.
 *
 * @param int|array $exportscrutari
 * @param string $type
 * @param int|array $ids
 * @param string $operation
 */
function exportscrutari_lier($exportscrutari, $type, $ids, $operation = 'add'){
	include_spip('inc/autoriser');
	include_spip('action/editer_liens');
	if (!$exportscrutari)
		$exportscrutari = "*";
	if (!$ids)
		$ids = array();
	elseif (!is_array($ids))
		$ids = array($ids);

	if ($operation=='del'){
		// on supprime les ids listes
		objet_dissocier(array('exportscrutari' => $exportscrutari), array($type => $ids));
	} else {
		// si c'est une affectation exhaustive, supprimer les existants qui ne sont pas dans ids
		// si c'est un ajout, ne rien effacer
		if ($operation=='set'){
			objet_dissocier(array('exportscrutari' => $exportscrutari), array($type => array("NOT", $ids)));
		}
		foreach ($ids as $id){
			objet_associer(array('exportscrutari' => $exportscrutari), array($type => $id));
		}
	}
}

/**
 * Supprimer un export
 *
 * @param int $id_export
 * @return int
 */
function exportscrutari_supprimer($id_exportscrutari){
	include_spip('action/editer_liens');
	objet_dissocier(array('exportscrutari' => $id_exportscrutari), array('*' => '*'));

	sql_delete("spip_exportscrutari", "id_exportscrutari=" . intval($id_exportscrutari));

	$id_exportscrutari = 0;
	return $id_exportscrutari;
}

?>
