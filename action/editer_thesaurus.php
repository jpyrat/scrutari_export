<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * editer un thesaurus (action apres creation/modif de thesaurus)
 *
 * @return array
 */
function action_editer_thesaurus_dist($arg = null){

	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!$id_thesaurus = intval($arg)){
		$id_thesaurus = thesaurus_inserer();
	}

	$err = thesaurus_modifier($id_thesaurus);

	return array($id_thesaurus, $err);
}

/**
 * Creer un nouveau thesaurus
 *
 * @return int
 */
function thesaurus_inserer(){

	include_spip('inc/autoriser');
	if (!autoriser('administrer', 'scrutariexport'))
		return false;

	$champs = array();
	// Envoyer aux plugins
	$champs = pipeline('pre_insertion',
		array(
			'args' => array(
				'table' => 'spip_thesaurus',
			),
			'data' => $champs
		)
	);
	$id_thesaurus = sql_insertq("spip_thesaurus", $champs);
	pipeline('post_insertion',
		array(
			'args' => array(
				'table' => 'spip_thesaurus',
				'id_objet' => $id_thesaurus
			),
			'data' => $champs
		)
	);
	return $id_thesaurus;
}

/**
 * Modifier un thesaurus
 *
 * @param int $id_thesaurus
 * @param array $set
 * @return string|bool
 */
function thesaurus_modifier($id_thesaurus, $set = null){

	include_spip('inc/modifier');
	$c = collecter_requests(
	// white list
		array('name', 'intitule_thesaurus'),
		// black list
		array(),
		// donnees eventuellement fournies
		$set
	);

	if ($err = objet_modifier_champs('thesaurus', $id_thesaurus,
		array(),
		$c)
	)
		return $err;

	thesaurus_lier($id_thesaurus, 'groupe', _request('groupes'), 'set');
	return $err;

}

/**
 * Mettre a jour les liens objets/thesaurus.
 *
 * @param int|array $thesaurus
 * @param string $type
 * @param int|array $ids
 * @param string $operation
 */
function thesaurus_lier($thesaurus, $type, $ids, $operation = 'add'){
	include_spip('inc/autoriser');
	include_spip('action/editer_liens');
	if (!$thesaurus)
		$thesaurus = "*";
	if (!$ids)
		$ids = array();
	elseif (!is_array($ids))
		$ids = array($ids);

	if ($operation=='del'){
		// on supprime les ids listes
		objet_dissocier(array('thesaurus' => $thesaurus), array($type => $ids));
	} else {
		// si c'est une affectation exhaustive, supprimer les existants qui ne sont pas dans ids
		// si c'est un ajout, ne rien effacer
		if ($operation=='set'){
			objet_dissocier(array('thesaurus' => $thesaurus), array($type => array("NOT", $ids)));
		}
		foreach ($ids as $id){
			if (autoriser('administrer', 'scrutariexport')){
				objet_associer(array('thesaurus' => $thesaurus), array($type => $id));
			}
		}
	}
}

/**
 * Supprimer un thesaurus
 *
 * @param int $id_thesaurus
 * @return int
 */
function thesaurus_supprimer($id_thesaurus){
	include_spip('action/editer_liens');
	objet_dissocier(array('thesaurus' => $id_thesaurus), array('*' => '*'));

	sql_delete("spip_thesaurus", "id_thesaurus=" . intval($id_thesaurus));

	$id_thesaurus = 0;
	return $id_thesaurus;
}

?>
